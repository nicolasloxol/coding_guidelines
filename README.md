# Coding guidelines

Welcome to my personal **C++** coding guidelines, a set of principles and practices I have curated to streamline my coding adventures.

Whether you are a fellow enthusiast or a seeker of coding wisdom, these guidelines aim to instill order and clarity into the vast realm of **C++**.

Within these pages, you will find the distilled essence of my coding philosophy. From naming conventions to code structure, these guidelines embody the lessons learned, the best practices embraced, and the quirks that make my code uniquely mine.

So, whether you are an explorer navigating my projects or just a curious mind, I invite you to dive into these guidelines.

## Naming conventions

In my coding endeavors, I consistently embrace the use of **CamelCase** for naming variables, functions, and other identifiers. This practice, where each word in a compound identifier starts with a capital letter (except the initial word), enhances the readability of my code and aligns with my preference for clear and concise naming conventions. By adhering to **CamelCase**, I aim to create code that is not only functional but also aesthetically pleasing and easy to follow.

### Prefixes

My **C++** coding style adhere to a consistent naming convention for variables to promote clarity and maintainability. Each variable type is prefixed with a designated letter to quickly convey its scope and purpose.

``` cpp
int         lPrice;  // local variable
std::string gDriver; // global variable
double      mSpeed;  // member variable
Object      pCar;    // parameter variable
```

This naming convention serves as a visual cue, aiding in the quick identification and understanding of variable roles within the codebase. Consistent use of these prefixes contributes to the overall readability and comprehension of the code.

### Functions and methods

Function and method names follow the **CamelCase** convention, ensuring a visually coherent style. For getters, I adopt a straightforward approach, symbolizing them with the name of the attribute they retrieve. On the other hand, setters use the *setAttribute* format, indicating their role in modifying an attribute's value.

When dealing with boolean attributes, the naming convention takes a slightly different path. Getter methods for boolean attributes adopt a predicate form, using *isAttribute* or *hasAttribute* to convey the query nature of these functions. This nuanced approach enhances code clarity by signaling that the function is checking a condition related to the boolean attribute.

``` cpp
class Car
{
    public:
        double speed() const;
        void   setSpeed(double pSpeed);

        bool isConvertible() const;
        void setConvertible(bool pConvertible);

        void drift();

    private:
        double mSpeed;
        bool   mConvertible;
};

void changeTires();
```

By consistently applying these conventions, I aim to create code that not only functions efficiently but is also intuitive and easy to understand.

## Code formatting

The codebase adheres to a consistent and standardized format to enhance readability and maintainability. I employ **clang-format**, a powerful tool for enforcing a unified code style.

The **.clang-format** file in the gitlab [Coding Guidelines repository](https://gitlab.com/nicolasloxol/coding_guidelines) specifies the formatting rules applied to the code.

## Comments

My coding guidelines prioritize comprehensive documentation using both **Doxygen** and traditional comments. **Doxygen** is employed for standard documentation, generating valuable insights directly from the source code. Additionally, developers are encouraged to use **C/C++** style comments, especially when detailing complex sections such as intricate functions or methods that may pose challenges for developers to understand. This dual approach ensures that the codebase remains well-documented, offering clarity and guidance both at the macro and micro levels. By combining **Doxygen**'s automated documentation capabilities with nuanced explanations in **C/C++** style comments, I foster a coding environment that values both automation and precision in documentation.

``` cpp
/**
 * @brief Compares two values of the same type.
 *
 * This function compares two values of the template type T and returns true if the first value
 * is greater than the second; otherwise, it returns false.
 *
 * @tparam T The type of values to be compared.
 *
 * @param pFirst The first value to be compared.
 * @param pSecond The second value to be compared.
 * @return true if the first value is greater than the second, false otherwise.
 *
 */
template<typename T>
bool compare(const T& pFirst, const T& pSecond)
{
    return pFirst > pSecond;
}
```

## Suggested conventions

In striving for well-organized and maintainable code, a set of suggested conventions is put forth within my coding standards.

### Modern C++ features

In my coding practices, I embrace the power of **C++** by leveraging its modern features and capabilities. Rather than adhering strictly to old **C-style** conventions, I strongly encourage the use of specific **C++** features that enhance code expressiveness, maintainability, and performance. By harnessing features such as **smart pointers**, **lambda expressions**, and **standard template library (STL) containers**, I aim to write code that not only adheres to **C++** standards but also takes full advantage of the language's evolving capabilities. This approach ensures that the codebase reflects a contemporary and efficient coding style, aligning with the evolving nature of the **C++** language.

### Standard file inclusion

In alignment with the commitment to modern **C++** practices, it is imperative that **C++** files refrain from using **C-style** includes. Instead, developers are mandated to utilize the corresponding **C++** include file for a given library or functionality. For instance, in lieu of *#include <cstdio>*, developers should opt for *#include <iostream>* when working with I/O in **C++**. This prohibition is in place to uphold the consistency and integrity of the codebase, ensuring that **C++** benefits from its designated features and optimizations. By adhering to this guideline, I maintain a standardized approach to include management, fostering a cleaner, more idiomatic **C++** codebase.

## Discouraged practices

In the pursuit of clean and maintainable code, certain practices are discouraged within my coding standards. These practices may lead to code that is error-prone or difficult to understand. It is advised to avoid the following:

### Spaghetti code

The use of the **goto** statement is strongly discouraged within my coding standards. While **goto** can be a powerful tool in certain scenarios, its misuse can lead to convoluted and hard-to-maintain code. The unstructured nature of **goto** can introduce spaghetti code, making it challenging to understand the flow of the program and increasing the likelihood of introducing bugs.

Instead of relying on **goto**, strive to use structured control flow constructs like **if**, **else**, **while**, **for**, and **functions** to achieve the desired logic. This promotes a more modular and readable codebase, aiding in easier debugging, maintenance, and collaboration.

Exceptions may exist where the use of **goto** is justified and well-documented; however, these cases should be rare and thoroughly explained.

### Consecutive conditional statement

To enhance code clarity and maintainability, it is advisable to limit the number of consecutive **if** statements. A maximum of three such statements in a row is recommended. If the logic requires more conditions, consider alternative structures such as **switch** statements or refactor the code to improve readability.

### Hardcoding magic numbers

In adherence to best coding practices, it is strongly advised to refrain from hardcoding magic numbers in the codebase. Magic numbers, arbitrary numerical constants embedded directly into the source code, can obscure the meaning of the values they represent and make the code less maintainable. Instead, utilize named constants or enumerations to give these numbers meaningful names, enhancing both code readability and maintainability.

### Long functions or methods

In accordance with our coding standards, it is recommended to avoid the creation of excessively long functions and methods. Long and monolithic code blocks can make the codebase challenging to understand, maintain, and debug. Instead, strive to break down complex logic into smaller, focused functions or methods, each addressing a specific concern.

My coding guidelines encouraged to limit the length of individual lines within functions or methods to a maximum of 50. 

### auto

The use of the **auto** keyword is generally discouraged, except in specific scenarios. The intent is to promote code clarity and explicit type declarations for variables. Instead of relying on **auto**, explicitly specify the variable type to enhance readability.

One notable and widely accepted use of the **auto** keyword is when declaring the return type of a lambda expression. Lambdas often involve complex or verbose types, and using **auto** for the return type can significantly improve code readability.

Nevertheless, it is essential to be mindful that the meaning and behavior of the **auto** keyword in **C++** can vary depending on the **C++** version. The evolution of the language introduces new features and refines existing ones, impacting how **auto** is interpreted.

By consequences, it is crucial to be aware of the **C++** version in use and understand how the **auto** keyword behaves accordingly.

## Enhanced code quality

In addition to adhering to modern **C++** practices, I strongly encourage developers to leverage extra tools that facilitate error prevention and enhance overall code quality.

In my commitment to producing high-quality code, I endorse the use of **Clang-Tidy**, a versatile static analysis tool. **Clang-Tidy** goes beyond basic linting by providing additional checks and suggestions to improve code correctness, maintainability, and performance. This tool helps catch potential issues and offers insights into modern **C++** best practices. Regular use of **Clang-Tidy** enhances code quality, facilitates adherence to coding standards, and contributes to a more robust and error-resistant codebase.

The **.clang-tidy** file in the gitlab [Coding Guidelines repository](https://gitlab.com/nicolasloxol/coding_guidelines) specifies the checks applied to the code.